/* 
	This file is part of emb-flashtool.

	emb-flashtool is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option) any later version.

	emb-flashtool is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with emb-flashtool.  If not, see <http://www.gnu.org/licenses/>.
*/
package emb.flashtool;

import android.content.*;
import android.os.*;
import android.util.Log;
import android.widget.*;
import java.util.*;
import android.net.*;
import android.database.*;
import android.graphics.*;
import android.provider.*;
import android.view.*;
import java.io.*;
import android.hardware.usb.*;
import com.hoho.android.usbserial.driver.*;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import tk.wasdennnoch.progresstoolbar.ProgressToolbar;
import okhttp3.*;

public class FlashActivity extends AppCompatActivity implements FlasherFragment.OnProgressChangedListener {

    private FlasherFragment fragment;

    public void onProgressChanged(boolean enable) {
        if(enable) {
            toolbar.showProgress();
        }else{
            toolbar.hideProgress();
        }
    }

    private BroadcastReceiver usbReceiver = new BroadcastReceiver() {
        public void onReceive(Context self, Intent intent) {
            fragment.removeAdapter();
        }
    };

    private final String TAG = FlashActivity.class.getSimpleName();

    private EventList.Logger logger;

    protected void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(usbReceiver, filter);
    }

    protected void onStop() {
        super.onStop();
        unregisterReceiver(usbReceiver);
    }

    private ProgressToolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null) {
            filename = savedInstanceState.getString(STATE_FN);
        }

        setContentView(R.layout.flash_layout);
        toolbar = (ProgressToolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#FFFFEBEE"));
        toolbar.setSubtitleTextColor(Color.parseColor("#FFFFEBEE"));
        if(filename == null) {
            toolbar.setSubtitle("No file");
        }else{
            toolbar.setSubtitle(filename);
        }
        toolbar.setBackgroundColor(Color.parseColor("#FFF44336"));
        toolbar.hideProgress();

        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#FFC62828"));
        }

        fragment = (FlasherFragment)getSupportFragmentManager().findFragmentById(R.id.flasher);
        logger = new EventList.Logger(TAG, fragment);
        Intent starter = getIntent();
        if(starter != null) {
            handleIntent(starter);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_browse:
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                startActivityForResult(intent, READ_REQUEST_CODE);
                return true;

            case R.id.action_refresh:
                refreshHttpUri();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private final static String STATE_FN = "fileName";
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString(STATE_FN, filename);
        super.onSaveInstanceState(savedInstanceState);
    }

    private void handleIntent(Intent intent) {
        if(intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
            fragment.refreshPort();
        }
        else if(intent.getAction().equals(Intent.ACTION_VIEW)) {
            Uri uri = intent.getData();
            Log.i(TAG, "file: " + uri.toString());
            loadFile(uri);
        }
        else if(intent.getAction().equals(Intent.ACTION_SEND)) {
            Uri streamurl = intent.getParcelableExtra(Intent.EXTRA_STREAM);
            String stringurl = intent.getStringExtra(Intent.EXTRA_TEXT);
            if(streamurl != null) {
                loadFile(streamurl);
            }else if(stringurl != null) {
                loadFile(Uri.parse(stringurl));
            }
        }
    }
    
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }


    private MenuItem menu_refresh;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_default, menu);
        menu_refresh = menu.findItem(R.id.action_refresh);
        if(httpUri != null) {
            menu_refresh.setVisible(true);
            menu_refresh.setEnabled(true);
        }
        return true;
    }

    private static final int READ_REQUEST_CODE = 42;
    private String filename;
    private void loadFileFromContent(Uri uri) {
        Cursor cursor = getContentResolver()
            .query(uri, null, null, null, null, null);

        try {
            if (cursor != null && cursor.moveToFirst()) {

                String displayName = cursor.getString(
                        cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                if(!displayName.endsWith(".hex")) {
                    logger.logError("Invalid file!");
                    return;
                }
                filename = displayName;
                toolbar.setSubtitle(displayName);
            }
        } finally {
            cursor.close();
        }
        try {
            InputStream inputStream = getContentResolver().openInputStream(uri);
            loadFile(inputStream);
        }catch(FileNotFoundException ex) {
            logger.logError("file not found", ex);
        }
    }
    private void loadFileFromLocal(Uri uri) {
        List<String> segments = uri.getPathSegments();
        filename = segments.get(segments.size()-1);
        toolbar.setSubtitle(filename);
        try {
            InputStream inputStream = new FileInputStream(uri.getPath());
            loadFile(inputStream);
        }catch(FileNotFoundException ex) {
            logger.logError("File not found!", ex);
            Log.i(TAG, "File not found!", ex);
        }
    }
    private void loadFile(Uri uri) {
        String scheme = uri.getScheme();
        if(scheme.equals(ContentResolver.SCHEME_CONTENT)) {
            loadFileFromContent(uri);
        }else if(scheme.equals(ContentResolver.SCHEME_FILE)) {
            loadFileFromLocal(uri);
        }else if(scheme.equals("http") || scheme.equals("https")) {
            loadFileFromHttp(uri);
        }else{
            Log.i(TAG, "????" + scheme);
        }
    }

    private OkHttpClient client = new OkHttpClient();

    private void refreshHttpUri() {
        if(menu_refresh != null) {
            menu_refresh.setEnabled(false);
        }
        fragment.refreshPort();
        final EventList.Event loader = logger.startIndeterminate("Loading");
        Request req = new Request.Builder()
            .url(httpUri.toString()).build();

        client.newCall(req).enqueue(new Callback() {
            public void onFailure(Call c, final IOException ex) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        logger.finishIndeterminate(loader);
                        logger.logError("Request failed!", ex);
                    }
                });
            }
            public void onResponse(Call c, final Response reply) {
                InputStream inputStream = reply.body().byteStream();
                if(!reply.header("Content-Type").equals("text/vnd.emb.hex")) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            logger.logError("Invalid mime type!",
                                    String.format("Type should be 'text/vnd.emb.hex', not '%s'", reply.header("Content-Type")));
                        }
                    });
                }else{
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                                inputStream));
                    final ArrayList<String> fileLines = new ArrayList<String>();
                    String line;
                    try {
                        while ((line = reader.readLine()) != null) {
                            fileLines.add(line);
                        }
                        inputStream.close();
                    }catch(final IOException ex) {
                        fileLines.clear();
                        runOnUiThread(new Runnable() {
                            public void run() {
                                logger.logError("Request failed!", ex);
                            }
                        });
                    }

                    List<String> segments = c.request().url().pathSegments();
                    filename = segments.get(segments.size()-1);
                    if(fileLines.size() > 0) {
                        fragment.loadFile(fileLines);
                    }
                    runOnUiThread(new Runnable() {
                        public void run() {
                            if(menu_refresh != null) {
                                menu_refresh.setEnabled(true);
                            }
                            toolbar.setSubtitle(filename);
                            logger.finishIndeterminate(loader);
                        }
                    });
                }
            }
        });
    }

    private Uri httpUri;
    private void loadFileFromHttp(Uri uri) {
        httpUri = uri;
        if(menu_refresh != null) {
            menu_refresh.setVisible(true);
            menu_refresh.setEnabled(true);
        }
        refreshHttpUri();
    }

    private void loadFile(InputStream inputStream) {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                        inputStream));
            ArrayList<String> fileLines = new ArrayList<String>();
            String line;
            while ((line = reader.readLine()) != null) {
                fileLines.add(line);
            }
            inputStream.close();
            fragment.loadFile(fileLines);
        }catch(IOException ex) {
            logger.logError("error reading file", ex);
        }
    }
    public void onActivityResult(int req, int res, Intent result) {
        if(req == READ_REQUEST_CODE && res == AppCompatActivity.RESULT_OK) {
            if(result != null) {
                Uri uri = result.getData();
                loadFileFromContent(uri);
            }
        }
    }
    private void hideFlashProgress() 
    {
        runOnUiThread(new Runnable() {
            public void run() {
                toolbar.hideProgress();
            }
        });
    }
}
