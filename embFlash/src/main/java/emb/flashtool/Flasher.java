/* 
	This file is part of emb-flashtool.

	emb-flashtool is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option) any later version.

	emb-flashtool is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with emb-flashtool.  If not, see <http://www.gnu.org/licenses/>.
*/
package emb.flashtool;
import java.io.*;
import java.util.*;
import android.util.*;
import android.os.Process;
import com.hoho.android.usbserial.driver.*;

public class Flasher {
    private enum FlashState { STATE_BLOAD, STATE_FLASH_DATA, STATE_START, STATE_IDLE, STATE_ERROR};
    private static final byte CMD_BLOAD = (byte)0xBB;
    private static final int CMD_DATA = 0xBD;
    private static final byte CMD_START = (byte)0xB5;


    private static final byte CMD_OK = (byte)0xB0;
    private static final byte CMD_GEN_ERROR = (byte)0xBE;
    private static final byte CMD_CRC_ERROR = (byte)0xBC;
    private static final byte CMD_WRT_ERROR = (byte)0xBF;

    private static final byte BLOCK_SIZE = 64;
    private static final byte BLOCK_CNT = 64;
    private static final byte BLOCK_MASK = 0x003F;
    private static final byte BLOCK_SHIFT = 6;

    private FlashState state;
    private static final String TAG = Flasher.class.getSimpleName();
    private UsbSerialPort port;
    private EventList.Logger logger;

    public Flasher(EventList.EventListHandler handler) {
        this.logger = new EventList.Logger(TAG, handler);
    }
    private void init() {
        state = FlashState.STATE_IDLE;
        flashCounter = 0;
    }
    public void open(UsbSerialPort port) throws IOException {
        this.port = port;
        init();
    }
    private void sleep(String msg, int ms) throws InterruptedException {
        this.logger.logDelay(msg, ms);
        Thread.sleep(ms);
    }
    public void flash() throws IOException, InterruptedException {
        port.setRTS(true);
        port.purgeHwBuffers(true,true);
        sleep("Waiting...", 1000);
        port.purgeHwBuffers(true,true);
        port.setRTS(false);
        writeCmd(CMD_BLOAD);
        state = FlashState.STATE_BLOAD;
        sleep("Waiting for bootloader...", 2000);
        while(handleReplies()){
        }
        Log.i(TAG, String.format("flash done? (%d)", Process.myTid()));
    }
    private boolean readReply() throws IOException {
        byte[] data = new byte[1];
        if(port.read(data, 1000) != 1) {
            logger.logError("Reply error!");
            return false;
        }
        switch(data[0]){
            case CMD_OK:
                return true;
            case CMD_GEN_ERROR:
                logger.logError("Generic error!");
                return false;
            case CMD_CRC_ERROR:
                logger.logError("CRC error!");
                return false;
            case CMD_WRT_ERROR:
                logger.logError("Flash write error!");
                return false;
            default: // Something fatal that should not happen.
                logger.logError(String.format("FATAL ERROR! - This shoud not happen. (%x)", data[0]));
                return false;
        }
    }

    int flashCounter;

    private void toErrorState() throws IOException {
        state = FlashState.STATE_ERROR;
    }

    private void toStartState() throws IOException {
        logger.logMessage("Starting application...");
        writeCmd(CMD_START);
        state = FlashState.STATE_START;
    }

    private EventList.Event current_loading;
    private void doFlashState() throws IOException {
        while(flashCounter < BLOCK_CNT && blocks[flashCounter] == null) {
            flashCounter++;
            current_loading = logger.updateLoading(current_loading, flashCounter);
        }
        if(flashCounter == BLOCK_CNT) {
            toStartState();
        } else if(blocks[flashCounter].send()) {
            flashCounter++;
            current_loading = logger.updateLoading(current_loading, flashCounter);
        }
    }

    private boolean handleReplies() throws IOException {
        boolean ret = true;
        switch(state) {
            case STATE_BLOAD:
                {
                    if(!readReply()) {
                        toErrorState();
                    }else{
                        current_loading = logger.logLoading("Writing blocks...", BLOCK_CNT);
                        state = FlashState.STATE_FLASH_DATA;
                        doFlashState();
                    }
                }break;
            case STATE_FLASH_DATA:
                {
                    if(!readReply()) {
                        toErrorState();
                    } else {
                        doFlashState();
                    }
                }break;
            case STATE_START:
                {
                    if(!readReply()) {
                        toErrorState();
                    } else {
                        logger.logMessage("Flashing done!");
                        state = FlashState.STATE_IDLE;
                    }
                }break;
            case STATE_IDLE:
                {
                    ret = false;
                }break;
            case STATE_ERROR:
                {
                    ret = false;
                }break;
        }
        return ret;
    }

    private class Block {
        short[] data;
        int addr;
        public Block(int idx) {
            data = new short[BLOCK_SIZE];
            addr = idx*BLOCK_SIZE>>1;
            for(int i = 0; i < data.length; i++) {
                data[i] = (short)(i%2 == 0? 0xFF: 0x3F);
            }
        }
        public void set(int offset, short[] array) {
            System.arraycopy(array, 0, data, offset, array.length);
        }
        public byte[] toSend() {
            byte[] sent = new byte[4+BLOCK_SIZE];
            sent[0] = (byte)CMD_DATA;
            int b_addr_l = addr & 0xFF;
            int b_addr_h = ((addr >> 8) & 0xFF);
            sent[1] = (byte)b_addr_l;
            sent[2] = (byte)b_addr_h;
            int sum = b_addr_l + b_addr_h;
            for(int i = 0; i < BLOCK_SIZE; i++) {
                sent[3+i] = (byte)data[i];
                sum += data[i];
            }
            sent[3+BLOCK_SIZE] = (byte)sum;
            return sent;
        }
        public boolean send() throws IOException {
            byte[] sent = toSend();
            boolean ok = (port.write(sent, 1000) == sent.length);
            if(!ok) {
                logger.logError("send error?");
            }
            return ok;
        }
        public int crc() {
            int b_addr_l = addr & 0xFF;
            int b_addr_h = ((addr >> 8) & 0xFF);
            int sum = b_addr_l + b_addr_h;
            for(int i = 0; i < BLOCK_SIZE; i++) {
                sum += data[i];
            }
            return sum % 256;
        }
    }
    private final static char[] hexArray = "0123456789abcdef".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    private Block[] blocks = new Block[BLOCK_CNT];
    private void setBlock(int page, int offset, short[] data) {
        if(blocks[page] == null) {
            blocks[page] = new Block(page);
        }
        blocks[page].set(offset, data);
        Log.i(TAG, "setBlock?");
    }


    private class Record {
        short[] data;
        int address;
        short type;
        short checksum;

        void storeInBlock() {
            if(!validate_addr(true)) {
                return;
            }

            int offset = address & BLOCK_MASK;
            int page = address >> BLOCK_SHIFT; 
            setBlock(page, offset, data);
        }

        boolean valid() {
            int sum = data.length + ((address >> 8) & 0xFF) +
                (address & 0xFF) + type + checksum;
            for(int i = 0; i<data.length; i++) {
                sum += 128+data[i];
            }
            return sum % 256 == 0;
        }

        boolean validate_addr() {
            return validate_addr(false);
        }
        boolean validate_addr(boolean silent) {
            int offset = address & BLOCK_MASK;
            int page = address >> BLOCK_SHIFT; 
            if((offset & 0x00F) != 0) {
                if(!silent) { logger.logError(String.format("invalid record %04x",address)); }
                return false;
            }
            if(address < 0x200) {
                if(!silent) { 
                    logger.logError(String.format("address reserved for bootloader %04x",address));
                }
                return false;
            }
            return true;
        }
    }

    public boolean loaded() {
        return loaded;
    }

    private void clearBlocks() {
        blocks = new Block[BLOCK_CNT];
    }

    public void load(ArrayList<String> lines) {
        Log.i(TAG, "why? " + Process.myTid());
        clearBlocks();
        for(String line: lines) {
            if(line.charAt(0) != ':') {
                logger.logError("invalid record in hex file");
            }else{
                Record r = new Record();
                int len = Integer.parseInt(line.substring(1,3), 16);
                r.data = new short[len];
                r.address = Integer.parseInt(line.substring(3,7), 16);
                r.type = Short.parseShort(line.substring(7,9), 16);
                int ll = line.length();
                r.checksum = Short.parseShort(line.substring(ll-2,ll),16);
                for(int i = 0; i < len; i++) {
                    int offs = 9+i*2;
                    r.data[i] = Short.parseShort(line.substring(offs, offs+2),16);
                }
                if(!r.valid()) {
                    logger.logError("invalid checksum (calc?)");
                    return;
                }
                if(r.type == 0 && r.data.length == 16) {
                    if(!r.validate_addr()) {
                        logger.logError("failed to store in block (calc?)");
                        return;
                    }else{
                        r.storeInBlock();
                    }
                }
            }
        }
        logger.logMessage("File loaded!");
        loaded = true;
    }
    private boolean loaded = false;

    private void writeCmd(byte cmd) throws IOException {
        byte[] data = { cmd };
        port.write(data, 200);
    }
}
