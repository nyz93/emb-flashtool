package emb.flashtool;

import android.app.*;
import android.os.*;
import android.view.*;
import android.hardware.usb.*;
import com.hoho.android.usbserial.driver.*;
import android.content.*;
import android.widget.*;
import java.util.*;
import java.io.*;
import android.util.Log;
import android.support.v4.app.Fragment;

public class FlasherFragment extends Fragment implements EventList.EventListHandler {
    private static final String TAG = FlasherFragment.class.getSimpleName();

    public void onEvent(final EventList.Event ev) {
        uiThread.post(new Runnable() {
            public void run() {
                boolean updated = false;
                for(int i = 0; i < events.size(); i++) {
                    EventList.Event old = events.get(i);
                    if(ev.updates(old)) {
                        events.set(i, ev);
                        updated = true;
                        break;
                    }
                }
                if(!updated) {
                    events.add(ev);
                }
                adapter.notifyDataSetChanged();
                list.smoothScrollToPosition(events.size()-1);
            }
        });
    }
    private Button flash;
    private Handler uiThread;
    private ArrayList<EventList.Event> events = new ArrayList<EventList.Event>();
    private LoggerAdapter adapter;

    private class LoggerAdapter extends ArrayAdapter<EventList.Event> {
        private Context context;
        public LoggerAdapter(Context ctx, ArrayList<EventList.Event> values) {
            super(ctx, -1, values);
            context = ctx;
        }
        public int getViewTypeCount() {
            return EventList.getTypeCount();
        }
        public int getItemViewType(int pos) {
            return getItem(pos).getViewTypeId();
        }
        public View getView(int pos, View old, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = old;
            EventList.Event ev = getItem(pos);
            if(rowView == null) {
                rowView = inflater.inflate(ev.getViewId(), parent, false);
            }
            ev.fillView(rowView);
            return rowView;
        }
    }

    private ListView list;
    private EventList.Logger logger;

    public void onCreate(Bundle saved) {
        super.onCreate(saved);
        setRetainInstance(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saved) {
        View layout = inflater.inflate(R.layout.event_list_fragment, container, false);
        flash = (Button)layout.findViewById(R.id.flash);
        if(flasher.loaded()) {
            flash.setVisibility(View.VISIBLE);
        }
        flash.setEnabled(!isFlashing()); // TODO check port
        flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { onFlashClick(); }
        });

        list = (ListView)layout.findViewById(R.id.logger);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View v, int pos, long id) {
                EventList.Event ev = (EventList.Event)adapter.getItemAtPosition(pos);
                ev.onClick();
            }
        });

        uiThread = new Handler();
        logger = new EventList.Logger(TAG, this);


        return layout;
    }
    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);
        adapter = new LoggerAdapter(ctx, events);
        progressListener = (OnProgressChangedListener)getActivity();
    }

    public void removeAdapter() {
        onRemovedAdapter();
        try {
            port.close();
            port = null;
        } catch(IOException ex) {
            // can't check if it's just an "already closed" exception
            // maybe there are no exceptions here
        } catch(Exception ex) {
            Log.i(TAG, "removeAdapter", ex);
        }
    }

    private void onRemovedAdapter() {
        flash.setText("no serial adapter");
        flash.setEnabled(false);
    }
    private void onHasAdapater() {
        flash.setText("flash");
        flash.setEnabled(true);
    }

    private Flasher flasher = new Flasher(this);
    private UsbSerialPort port;

    private boolean isFlashing() {
        return flasherThread != null && flasherThread.isAlive();
    }

    public void refreshPort() {
        if(!isFlashing()) {
            if(port != null) {
                try {
                    port.close();
                }catch(Exception ex){
                    Log.i(TAG, "refreshPort", ex);
                }
            }
            Log.i(TAG, "refreshing port");
            UsbManager manager = (UsbManager) getActivity().getSystemService(Context.USB_SERVICE);
            List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().findAllDrivers(manager);
            if (availableDrivers.isEmpty()) {
                onRemovedAdapter();
                return;
            }
            UsbSerialDriver driver = availableDrivers.get(0);
            UsbDeviceConnection connection = manager.openDevice(driver.getDevice());
            if(connection == null) {
                logger.logError("Port has to be associated with app!");
                return;
            }
            port = driver.getPorts().get(0);
            try {
                port.open(connection);
                port.setParameters(9600, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
                flasher.open(port);
                onHasAdapater();
            }catch(IOException ex) {
                logger.logError("Error opening port!", ex);
            }
        }
    }

    public void loadFile(ArrayList<String> lines) { // TODO Uri
        events.clear();
        flash.setVisibility(View.VISIBLE);
        flasher.load(lines);
    }
    
    private void setFlashProgress(final boolean isFlashing) {
        uiThread.post(new Runnable() {
            public void run() {
                flash.setEnabled(!isFlashing);
                progressListener.onProgressChanged(isFlashing);
            }
        });
    }
    private Thread flasherThread;
    public void onFlashClick() {
        flasherThread = new Thread(new Runnable() {
            public void run() {
                try {
                    setFlashProgress(true);
                    flasher.flash();
                }catch(InterruptedException ex) {
                    Log.e(TAG, "ERROR", ex);
                }catch(Exception ex) {
                    Log.e(TAG, "ERROR", ex);
                }finally{
                    setFlashProgress(false);
                }
            }
        });
        flasherThread.start();
    }

    private OnProgressChangedListener progressListener;
    public interface OnProgressChangedListener {
        public void onProgressChanged(boolean enable);
    }

}
