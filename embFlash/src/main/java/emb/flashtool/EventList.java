/* 
	This file is part of emb-flashtool.

	emb-flashtool is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option) any later version.

	emb-flashtool is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with emb-flashtool.  If not, see <http://www.gnu.org/licenses/>.
*/
package emb.flashtool;
import android.widget.*;
import android.view.*;
import android.view.animation.*;
import android.util.*;
import java.util.*;
import android.animation.*;

import com.github.lzyzsd.circleprogress.DonutProgress;

public class EventList {

    private static List<Class<?>> _types;
    private static int getTypeId(Class<?> cl) {
        if(_types == null) {
            getTypeCount();
        }
        int ret = _types.indexOf(cl);
        if(ret < 0) {
            throw new RuntimeException(String.format("%s is not an event type!", cl));
        }
        return ret;
    }

    public static abstract class Event {
        public abstract int getViewId();
        public abstract int getViewTypeId();
        public abstract void fillView(View v);
        public void onClick() {
        }
        protected Event old;
        public boolean updates(Event old) {
            return this.old == old;
        }
    }
    public synchronized static int getTypeCount() {
        if(_types == null) {
            _types = new ArrayList<Class<?>>();
            Class<?>[] classes = EventList.class.getDeclaredClasses();
            for(Class<?> cl: classes) {
                if(cl.getSuperclass() == Event.class) {
                    _types.add(cl);
                }
            }
        }
        return _types.size();
    }
    private static class LogMessage extends Event {
        private String msg;
        public LogMessage(String msg) {
            this.msg = msg;
        }
        public int getViewTypeId() {
            return getTypeId(LogMessage.class);
        }
        public int getViewId() {
            return R.layout.logger_item_message;
        }
        public void fillView(View v) {
            TextView t = (TextView)v.findViewById(R.id.logger_item_label);
            t.setText(msg);
        }
    }
    private static class LogError extends Event {
        private String msg;
        private String err;
        public LogError(String msg) {
            this.msg = msg;
        }
        public LogError(String msg, String ex) {
            this.msg = msg;
            this.err = ex;
        }
        public LogError(String msg, Exception ex) {
            this.msg = msg;
            this.err = Log.getStackTraceString(ex);
        }
        public int getViewTypeId() {
            return getTypeId(LogError.class);
        }
        public int getViewId() {
            return R.layout.logger_item_error;
        }
        private TextView error;
        public void fillView(View v) {
            TextView t = (TextView)v.findViewById(R.id.logger_item_label);
            t.setText(msg);
            if(err != null) {
                error = (TextView)v.findViewById(R.id.logger_item_error);
                error.setText(err);
            }
            
        }
        public void onClick() {
            if(error != null) {
                if(error.getVisibility() == View.GONE) {
                    error.setVisibility(View.VISIBLE);
                }else{
                    error.setVisibility(View.GONE);
                }
            }
        }
    }
    public static class LogLoading extends Event {
        public int getViewTypeId() {
            return getTypeId(LogLoading.class);
        }
        protected int max;
        protected int set;
        protected String msg;
        public static LogLoading update(LogLoading old, int set) {
            LogLoading ret = new LogLoading(old.msg, old.max);
            ret.old = old;
            ret.set = set;
            return ret;
        }
        public LogLoading(String msg, int max) {
            this.msg = msg;
            this.max = max;
        }
        public void fillView(View v) {
            Log.i("EventList", "delay");
            TextView t = (TextView)v.findViewById(R.id.logger_item_label);
            t.setText(msg);
            DonutProgress progress = (DonutProgress)v.findViewById(R.id.logger_item_loading_progress);
            if(this.set == this.max) {
                progress.setVisibility(View.GONE);
                ImageView done = (ImageView)v.findViewById(R.id.logger_item_loading_done);
                done.setVisibility(View.VISIBLE);
            }else{
                progress.setMax(this.max);
                progress.setProgress(this.set);
            }
        }
        public int getViewId() {
            return R.layout.logger_item_loading;
        }
    }

    private static class IndeterminateEvent extends Event {
        public int getViewId() {
            return R.layout.logger_item_indeterminate;
        }
        public int getViewTypeId() {
            return getTypeId(IndeterminateEvent.class);
        }
        public IndeterminateEvent(String msg) {
            this.msg = msg;
        }
        private String msg;
        public static IndeterminateEvent finish(IndeterminateEvent old) {
            IndeterminateEvent ret = new IndeterminateEvent(old.msg);
            ret.old = old;
            ret.isDone = true;
            return ret;
        }
        private boolean isDone = false;
        public void fillView(View v) {
            TextView t = (TextView)v.findViewById(R.id.logger_item_label);
            t.setText(msg);
            if(isDone) {
                ProgressBar progress = (ProgressBar)v.findViewById(R.id.logger_item_progress);
                progress.setVisibility(View.GONE);
                ImageView done = (ImageView)v.findViewById(R.id.logger_item_loading_done);
                done.setVisibility(View.VISIBLE);
            }
        }
    }

    private static class DelayEvent extends LogLoading implements Animator.AnimatorListener {
        private int duration;
        private boolean do_anim = true;
        private boolean done_anim = false;
        public DelayEvent(String label, int duration) {
            super(label, 100);
            this.duration = duration;
        }
        public void onAnimationStart(Animator animation) {
            p.setVisibility(View.VISIBLE);
            done.setVisibility(View.GONE);
        }
        public void onAnimationCancel(Animator animation) {
        }
        public void onAnimationRepeat(Animator animation) {
        }
        public void onAnimationEnd(Animator anim) {
            p.setVisibility(View.GONE);
            done.setVisibility(View.VISIBLE);
            done_anim = true;
        }
        DonutProgress p;
        ImageView done;

        @Override
        public void fillView(View v) {
            TextView t = (TextView)v.findViewById(R.id.logger_item_label);
            t.setText(msg);
            p = (DonutProgress)v.findViewById(R.id.logger_item_loading_progress);
            done = (ImageView)v.findViewById(R.id.logger_item_loading_done);
            if(do_anim) {
                do_anim = false;
                ObjectAnimator animate = ObjectAnimator.ofFloat(p, "progress", 0, 100);
                animate.addListener(this);
                animate.setDuration(duration);
                animate.start();
            }
            if(done_anim) {
                p.setVisibility(View.GONE);
                done.setVisibility(View.VISIBLE);
            }
        }
    }

    public static interface EventListHandler {
        public void onEvent(Event ev);
    }

    public static class Logger {
        private EventListHandler handler;
        private String tag;
        public Logger(String tag, EventListHandler handler) {
            this.tag = tag;
            this.handler = handler;
        }
        public void logMessage(String msg) {
            this.handler.onEvent(new LogMessage(msg));
        }
        public void logError(String msg, Exception ex) {
            this.handler.onEvent(new LogError(msg, ex));
        }
        public void logError(String msg, String desc) {
            this.handler.onEvent(new LogError(msg, desc));
        }
        public void logError(String msg) {
            this.handler.onEvent(new LogError(msg));
        }
        public void logDelay(String msg, int delay) {
            this.handler.onEvent(new DelayEvent(msg, delay));
        }
        public Event logLoading(String msg, int max) {
            Event ev = new LogLoading(msg, max);
            this.handler.onEvent(ev);
            return ev;
        }
        public Event updateLoading(Event old, int set) {
            if(old instanceof LogLoading) {
                Event ev = LogLoading.update((LogLoading)old, set);
                this.handler.onEvent(ev);
                return ev;
            }else{
                return null;
            }
        }
        public Event startIndeterminate(String msg) {
            Event ev = new IndeterminateEvent(msg);
            this.handler.onEvent(ev);
            return ev;
        }
        public Event finishIndeterminate(Event old) {
            if(old instanceof IndeterminateEvent) {
                Event ev = IndeterminateEvent.finish((IndeterminateEvent)old);
                this.handler.onEvent(ev);
                return ev;
            }else{
                return null;
            }
        }
    }
}
